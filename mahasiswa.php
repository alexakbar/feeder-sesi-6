<?php
session_start();
include_once('Connection/db.php'); 
include_once('Controller/UnivController.php'); 
include_once('Controller/ProdiController.php');
include_once('Controller/ReservasiController.php');
include_once('Controller/PemasanganController.php');

use Controller\PemasanganController;
use Controller\ProdiController;
use Controller\ReservasiController;
use Controller\UnivController;

global $conn;

// get kode prodi dan univ
$kodeProdi = $_POST['prodi'];
$univ = $_POST['univ'];

// get type pin
$type = $_SESSION["type"];

if ($type == 'Reservasi') {
    // get Controller reservasi
    $reservasi = new ReservasiController($conn);
    $a_MhsReservasi = $reservasi->getMahasiswa($univ,$kodeProdi);
} else {
    // get Controller pemasangan
    $pemasangan = new PemasanganController($conn);
    $a_MhsReservasi = $pemasangan->getMahasiswa($univ,$kodeProdi);
}

// get univ_name
$c_univ = new UnivController($conn);
$univ_name = $c_univ->findUniv($univ);

// get prodi
$prodi = new ProdiController($conn);
$prodi_name = $prodi->findProdi($univ,$kodeProdi);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PIN PDDikti</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />
    
    <style>
        .center{
            text-align: center;
        }
    </style>
</head>
<body>
<div class="container">
    <h1 class="center"><?= $type ?> PIN</h1>
    <h5 class="center"><?= $univ_name ?> > <?= $prodi_name ?></h5>
    <br>
    <div>
        <a href="prodi.php" class="btn btn-info" style="float: right;">Kembali</a>
    </div>
    <div class="eligible">
        <h4>Eligible</h4>
        <br>

        <table id="eligible" class="display" cellspacing="0" width="100%">
            <thead>
                <th width="4%">No</th>
                <th>Nama</th>
                <th>NIM</th>
                <th>SKS</th>
                <th>IPK</th>
                <th>Alasan</th>
            </thead>
            <tfoot>
                <th width="4%">No</th>
                <th>Nama</th>
                <th>NIM</th>
                <th>SKS</th>
                <th>IPK</th>
                <th>Alasan</th>
            </tfoot>
            <tbody>
                <?php 
                $no = 1;
                foreach ($a_MhsReservasi['eligible'] as $key => $data) { ?>
                    <tr>
                        <td><?= $no++ ?></td>
                        <td><?= $data['nama'] ?></td>
                        <td><?= $data['nim'] ?></td>
                        <td><?= $data['total_sks'] ?></td>
                        <td><?= $data['ipk'] ?></td>
                        <td>OK</td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>

    <br>
    <br>
    <div class="not-eligible">
        <h4>Tidak Eligible</h4>
        <br>

        <table id="not-eligible" class="display" cellspacing="0" width="100%">
            <thead>
                <th width="4%">No</th>
                <th>Nama</th>
                <th>NIM</th>
                <th>SKS</th>
                <th>IPK</th>
                <th>Alasan</th>
            </thead>
            <tfoot>
                <th width="4%">No</th>
                <th>Nama</th>
                <th>NIM</th>
                <th>SKS</th>
                <th>IPK</th>
                <th>Alasan</th>
            </tfoot>
            <tbody>
                <?php 
                $no = 1;
                foreach ($a_MhsReservasi['not_eligible'] as $key => $data) { ?>
                    <tr>
                        <td><?= $no++ ?></td>
                        <td><?= $data['nama'] ?></td>
                        <td><?= $data['nim'] ?></td>
                        <td><?= $data['total_sks'] ?></td>
                        <td><?= $data['ipk'] ?></td>
                        <td><?= implode(', ', $data['alasan']) ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script>
    $('#eligible').DataTable();
    $('#not-eligible').DataTable();  
</script>
</body>
</html>