<?php
session_start();
include_once('Connection/db.php'); 
include_once('Controller/UnivController.php'); 

use Controller\UnivController;

global $conn;

$univ = new UnivController($conn);
$a_univ = $univ->getUniv();

if (empty($_SESSION['type']))
    $_SESSION["type"] = $_POST['type'];

$type = $_SESSION['type'];
if(empty($type)){
    // kembali ke halaman utama
    header("location: index.php");
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PIN PDDikti</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <style>
        .row h1{
            text-align: center;
        }
        .button-submit{
            margin-top: 12px;
        }
        label{
            margin-bottom: 12px;
        }
        select{
            padding: 10px 0px;
        }
    </style>
</head>
<body>
<div class="container">
    <form action="prodi.php" method="POST">
        <div class="row">
            <h1><?= $type ?> PIN</h1>
            <div>
                <a href="index.php" class="btn btn-info" style="float: right;">Kembali</a>
            </div>
            <div class="col-md-6">
                <label for="">Prodi</label>
                <select class="form-control" name="univ" id="univ">
                    <?php foreach($a_univ as $id => $univ) { ?>
                        <option value="<?= $id ?>"><?= $univ ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="button-submit">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
    $("#univ").select2({
        placeholder: "Pilih Univ"
    });
</script>
</body>
</html>