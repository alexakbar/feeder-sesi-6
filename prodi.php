<?php
session_start();
include_once('Controller/ProdiController.php'); 
include_once('Controller/UnivController.php'); 
include_once('Connection/db.php'); 

use Controller\ProdiController;
use Controller\UnivController;

global $conn;

if (empty($_SESSION['univ']))
    $_SESSION['univ'] = $_POST['univ'];

$univ = $_SESSION['univ'];
if(empty($univ)){
    // kembali ke halaman utama
    header("location: index.php");
    exit();
}

// get Controller
$prodi = new ProdiController($conn);
$a_prodi = $prodi->getProdi($univ);

$c_univ = new UnivController($conn);
$univ_name = $c_univ->findUniv($univ);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PIN PDDikti</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style>
        .row h1,h5{
            text-align: center;
        }
        .button-submit{
            margin-top: 12px;
        }
        label{
            margin-bottom: 12px;
        }
    </style>
</head>
<body>
<div class="container">
    <form action="mahasiswa.php" method="POST">
        <div class="row">
            <h1><?= $_SESSION["type"] ?> PIN</h1>
            <h5><?= $univ_name ?></h5>
            <div>
                <a href="univ.php" class="btn btn-info" style="float: right;">Kembali</a>
            </div>
            <div class="col-md-6">
                <label for="">Prodi</label>
                <select class="form-control" name="prodi" id="prodi">
                    <?php foreach($a_prodi as $row) { ?>
                        <option value="<?= $row['kode'] ?>"><?= $row['prodi'] ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-6">
                <input type="hidden" name="univ" value="<?= $univ ?>">
            </div>
            <div class="button-submit">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
    
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>
</html>