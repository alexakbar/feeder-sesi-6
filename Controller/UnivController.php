<?php
namespace Controller;

class UnivController
{

    private $conn;

    public function __construct($conn) {
        $this->conn = $conn;
    }

    public function getUniv()
    {
        $sql = "SELECT * FROM public.satuan_pendidikan WHERE nm_lemb ilike '%Universitas Tadulako%' LIMIT 100";

        $result = pg_query($this->conn, $sql);
        $results = [];
        while ($row = pg_fetch_assoc($result)) {
            $results[$row['id_sp']] = $row['nm_lemb'];
        }

        return $results;
    }

    public function findUniv($iduniv)
    {
        $sql = "SELECT nm_lemb FROM public.satuan_pendidikan WHERE id_sp = '".$iduniv."'";
        $result = pg_query($this->conn, $sql);
        $row = pg_fetch_row($result);
        return $row[0];
    }
}
