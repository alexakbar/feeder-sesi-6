<?php
namespace Controller;

class ProdiController
{

    private $conn;

    public function __construct($conn) {
        $this->conn = $conn;
    }

    public function getProdi($iduniv)
    {
        $sql = "SELECT jp.nm_jenj_didik || ' - ' || p.nm_lemb as prodi, p.kode_prodi
                FROM public.sms p
                join ref.jenjang_pendidikan jp using (id_jenj_didik)
                where id_jns_sms = '3' and p.id_sp = '".$iduniv."'";
        
        $result = pg_query($this->conn, $sql);
        $results = [];
        $key = 0;
        while ($row = pg_fetch_assoc($result)) {
            $results[$key]['prodi'] = $row['prodi'];
            $results[$key]['kode'] = $row['kode_prodi'];
            $key++;
        }
        return $results;
    }

    public function findProdi($iduniv,$kodeprodi)
    {
        $sql = "SELECT nm_lemb FROM public.sms WHERE id_sp = '".$iduniv."' and kode_prodi = '".$kodeprodi."'";
        $result = pg_query($this->conn, $sql);
        $row = pg_fetch_row($result);
        return $row[0];
    }
}
