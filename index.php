<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PIN PDDikti</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <style>
        .row h1{
            text-align: center;
        }
        .button-submit{
            margin-top: 12px;
        }
        label{
            margin-bottom: 12px;
        }
        select{
            padding: 10px 0px;
        }
    </style>
</head>
<body>
<div class="container">
    <h1>Halaman PIN</h1>
    <br>

    <div style="display: flex;">
        <form action="univ.php" method="POST">
            <input type="submit" value="Pemasangan" name="type">
        </form>
        <form action="univ.php" method="POST" style="margin-left:12px">
            <input type="submit" value="Reservasi" name="type">
        </form>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>
</html>